@extends('auth.layouts.master')

@section('content')
    <form action="{{ route('shops.update', $shop) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf

        <h1 class="mt-5">Редактирование магазина</h1>

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" placeholder="Введите название магазина" id="name" name="name" value="{{ $shop->name}}">
        </div>

        <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" class="form-control" id="address" name="address" placeholder="Введите адресс" value="{{ $shop->address }}">
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" placeholder="Введите описание" id="description" name="description" value="{{ $shop->description }}">
        </div>

        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" placeholder="" id="" name="">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
