@extends('auth.layouts.master')

@section('content')
    @foreach($shops as $shop)
        <h1>{{ $shop->name }}</h1>
        <h2>{{ $shop->description }}</h2>
        <h2>{{ $shop->address }}</h2>
        <img src="http://internet-shop.tmweb.ru/storage/products/iphone_x.jpg">
        <div class="container">
            <form action="{{ route('shops.destroy', $shop) }}" method="POST">
                <a class="btn btn-success" type="button"
                   href="{{ route('shops.show', $shop) }}">Открыть</a>
                <a class="btn btn-warning" type="button"
                   href="{{ route('shops.edit', $shop) }}">Редактировать</a>
                @csrf
                @method('DELETE')
                <input class="btn btn-danger" type="submit" value="Удалить"></form>
        </div>
    @endforeach
@endsection
