@extends('auth.layouts.master')

@section('content')
    <h1>Название категории</h1>
    <h2>{{ $product->name }}</h2>
    <h2>{{ $product->category->name }}</h2>
    <p>Цена: <b>{{ $product->price }} руб.</b></p>
    <p>{{ $product->description }}</p>
    @if($product->image)
        <div class="row">
            <div class="col-m-12">
                <img src="{{ asset('storage/' . $product->image) }}"  alt="" class="img-thumbnail">
            </div>
        </div>
    @endif
@endsection
