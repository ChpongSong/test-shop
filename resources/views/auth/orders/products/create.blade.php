@extends('auth.layouts.master')

@section('content')
    <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <h1 class="mt-5">Создание товара</h1>

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" placeholder="Введите название товара" id="name" name="name" value="{{ old('name') }}">
        </div>

        <div class="form-group">
            <label for="code">Code:</label>
            <input type="text" class="form-control" id="code" name="code" value="{{ old('code') }}">
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" placeholder="" id="description" name="description" value="{{ old('description') }}">
        </div>

        <div class="form-group">
            <label for="price">Price:</label>
            <input type="number" class="form-control" placeholder="" id="price" name="price" min="1" value="{{ old('price') }}">
        </div>

        <div class="form-group">
            <label for="categories">Выбор категории</label>
            <select class="form-control" id="categories" name="category_id">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" placeholder="" id="" name="">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
