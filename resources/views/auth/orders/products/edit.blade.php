@extends('auth.layouts.master')

@section('content')
    <form action="{{ route('products.update', $product) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf

        <h1 class="mt-5">Редактирование товара</h1>

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" placeholder="Введите название товара" id="name" name="name" value="{{ $product->name }}">
        </div>

        <div class="form-group">
            <label for="code">Code:</label>
            <input type="text" class="form-control" id="code" name="code" value="{{ $product->code }}">
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" placeholder="" id="description" name="description" value="{{ $product->description }}">
        </div>

        <div class="form-group">
            <label for="price">Price:</label>
            <input type="number" class="form-control" placeholder="" id="price" name="price" min="1" value="{{ $product->price }}">
        </div>

        <div class="form-group">
            <label for="categories">Выбор категории</label>
            <select class="form-control" id="categories" name="category_id">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="">
            <label for="image">Image:</label>
            <input type="file" class="form-control" placeholder="" id="image" name="image">
            <div> {{ $errors->first('image') }}</div>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
