@extends('auth.layouts.master')

@section('content')
    <h1 class="mt-5">Создание категории</h1>
    <form action="{{ route('categories.store') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" placeholder="Введите название категории" name="name" id="name" value="{{ old('name') }}">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" class="form-control" placeholder="Введите описание" name="description" id="description" value="{{ old('description') }}">
        </div>
        <div class="form-group">
            <label for="">Image</label>
            <input type="file" class="form-control" placeholder="" id="">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
