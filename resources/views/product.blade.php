@extends('layout.master')

@section('title', 'Товар')

@section('content')
    @if($product)
        <h1>{{ $product->name }}</h1>
        <p>Цена: <b>{{ $product->price }} руб.</b></p>
        <img src="/storage/products/iphone_x.jpg">
        <p>{{ $product->description }}</p>
    @endif
@endsection
