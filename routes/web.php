<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('get-logout');
    Route::get('/', 'MainController@index')->name('index');

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function (){
        Route::get('/', 'OrderController@index')->name('panel');
        Route::resource('categories', 'CategoryController');
        Route::resource('products', 'ProductController');
        Route::resource('shops', 'ShopController');
    });

    Route::group(['prefix' => 'basket'], function (){
        Route::get('/', 'BasketController@basket')->name('basket');
        Route::get('/place', 'BasketController@basketPlace')->name('basket-place');
        Route::post('/add/{id}', 'BasketController@basketAdd')->name('basket-add');
        Route::post('/confirm', 'BasketController@basketConfirm')->name('basket-confirm');
        Route::post('/remove/{id}', 'BasketController@basketRemove')->name('basket-remove');
    });
    Route::get('/categories', 'MainController@categories')->name('categories');
    Route::get('/{category}', 'MainController@category')->name('category');
    Route::get('/{category}/product', 'MainController@product')->name('product');
});


