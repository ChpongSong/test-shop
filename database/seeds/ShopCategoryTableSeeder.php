<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShopCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shopCategory = [];

        for ($i = 1; $i  < 5; $i++) {
            $shopCategory[$i]['shop_id'] = rand(1, 2);
            $shopCategory[$i]['category_id'] = rand(1, 5);
        }

        DB::table('category_shop')->insert($shopCategory);
    }
}
