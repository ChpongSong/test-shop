<?php

use App\Models\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Str;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $products = [];

        for ($i = 1; $i < 11; $i++) {
            $products[$i]['category_id'] = rand(1, 3);
            $products[$i]['name'] = $faker->name;
            $products[$i]['code'] = Str::slug($products[$i]['name']);
            $products[$i]['description'] = $faker->text;
            $products[$i]['price'] = rand(200, 1000);
        }


        Product::query()->insert($products);
    }
}
