<?php

use App\Models\Shop;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $shops = [
            [
                'address' => $faker->address,
                'description' => $faker->text,
                'name' => $faker->name
            ],
            [
                'address' => $faker->address,
                'description' => $faker->text,
                'name' => $faker->name
            ]
        ];

        Shop::query()->insert($shops);
    }
}
