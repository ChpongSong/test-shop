<?php

use App\User;
use Illuminate\Database\Seeder;

class CreateAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => bcrypt('281749799s'),
            'user_role' => 'admin'
        ];

        User::query()->insert($admin);
    }
}
