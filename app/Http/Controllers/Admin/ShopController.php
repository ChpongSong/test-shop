<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\StoreShopRequest;
use App\Http\Requests\Shop\UpdateShopRequest;
use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::query()->select('id', 'address', 'description', 'name', 'image')->get();
        return view('auth.orders.shops.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.orders.shops.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShopRequest $request)
    {
        Shop::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'description' => $request->get('description')
        ]);
        return redirect()->route('shops.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = Shop::query()->where('id', $id)->select('id', 'address', 'description', 'name', 'image')->first();
        return view('auth.orders.shops.show', compact('shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::query()->where('id', $id)->select('id', 'address', 'description', 'name', 'image')->first();
        return view('auth.orders.shops.edit', compact('shop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShopRequest $request, $id)
    {
        Shop::query()->where('id', $id)->update([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'address' => $request->get('address')
        ]);
        return redirect()->route('shops.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Shop::query()->where('id', $id)->delete();
        return back();
    }
}
