<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if ($user->can('checkOrders', User::class)) {
            $orders = Order::query()->where('status', 1)->get();
            return view('auth.orders.index', compact('orders'));
        }
        session()->flash('warning', 'У вас недостаточно уровня доступа в эту секцию');
       return redirect()->route('index');
    }
}
