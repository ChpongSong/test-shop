<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    public function basket()
    {
        $orderId = session('orderId');
        if (!is_null($orderId)) {
            $order = Order::findOrFail($orderId);
            return view('basket', compact('order'));
        }
        session()->flash('warning', 'Ваша корзина пуста');
        return back();
    }

    public function basketPlace()
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('index');
        }
        $order = Order::find($orderId);
        return view('order', compact('order'));
    }

    public function basketConfirm(Request $request)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('index');
        }
        $order = Order::find($orderId);
        $success = $order->saveOrder($request->name, $request->phone);
        if ($success) {
            session()->flash('success', 'Ваш заказ принят в обработку');
        } else {
            session()->flash('warning', 'Случилась ошибка');
        }
        return redirect()->route('index');
    }

    public function basketAdd($productId)
    {
        $user = Auth::user();
        $orderId = session('orderId');
        if (is_null($orderId)) {
            $orderId = Order::create()->id;;
            session(['orderId' => $orderId]);
            $order = Order::query()->where('id', $orderId)->first();
        } else {
            $order = Order::find($orderId);
        }
        if ($order->products->contains($productId)) {
            $pivatRow = $order->products()->where('product_id', $productId)->first()->pivot;
            $pivatRow->count++;
            $pivatRow->update();
        } else {
            $order->products()->attach($productId);
        }
        $product = Product::find($productId);
        session()->flash('success',' Добавлен товар '. $product->name);

        return redirect()->route('basket');
    }

    public function basketRemove($productId)
    {
        $orderId = session('orderId');
        if (is_null($orderId)) {
            return redirect()->route('basket');
        }
        $order = Order::find($orderId);
        if ($order->products->contains($productId)) {
            $pivatRow = $order->products()->where('product_id', $productId)->first()->pivot;
            if ($pivatRow->count < 2) {
                $order->products()->detach($productId);
            }
            else {
                $pivatRow->count--;
                $pivatRow->update();
            }
        }
        $product = Product::query()->where('id', $productId)->first();
        session()->flash('warning',' Ваш заказ удален ' . $product->name);
        return redirect()->route('basket');
    }
}
