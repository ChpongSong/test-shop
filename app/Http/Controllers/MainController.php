<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $products = Product::query()->get();
        return view('index', compact('products'));
    }

    public function categories()
    {
        $categories = Category::query()->get();
        return view('categories', compact('categories'));
    }

    public function category($code)
    {
        $category = Category::query()->where('code', $code)->first();
        return view('category', compact('category'));
    }

    public function product($category, $product = null)
    {
        $category = Category::query()->where('code', $category)->first();
        $product = Product::query()->where('id', $category->id)->first();
        return view('product', compact( 'product'));
    }


}
